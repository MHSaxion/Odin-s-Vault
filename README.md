# Odin's Vault

Odin's Vault is geïnspireerd door de Noorse mythologie, met Odin als symbool van kennis en bescherming. Het doel is om een veilige omgeving te creëren waarin ik al mijn wachtwoorden en gevoelige informatie kan opslaan.

## Wireframes

Hieronder vind je de wireframes voor de verschillende pagina's van het project:

1. [LoginPage](https://wireframe.cc/U9ldI3)
2. [RegisterPage](https://wireframe.cc/S5USyX)
3. [Dashboard](https://wireframe.cc/S9z9jv)
4. [Password Changes](https://wireframe.cc/Oana80)
5. [User Settings](https://wireframe.cc/bUwqxu)
